
const sum = require('./main')

test(`Testcase 1`, () => {
    expect(sum.summary("123","4565")).toBe("4688");
})

test(`Testcase 2`, () => {
    expect(sum.summary("456789","4565")).toBe("461354");
})

test(`Testcase 3`, () => {
    expect(sum.summary("character","4565")).toBe(false);
})

test(`Testcase 3`, () => {
    expect(sum.summary("","4565")).toBe("4565");
})
